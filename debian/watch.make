version=4
opts=filenamemangle=s/.+\/v?(\d\S*)\.tar\.gz/golang-github-mattn-go-gtk-\$1\.tar\.gz/,\
uversionmangle=s/(\d)[_\.\-\+]?(RC|rc|pre|dev|beta|alpha)[.]?(\d*)$/\$1~\$2\$3/ \
  https://github.com/mattn/go-gtk/tags .*/v?(\d\S*)\.tar\.gz
